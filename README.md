# README #

Repository für die erste Präsentation des Erosionsprojektes.

## Layout ##
Als Layout liegt das Paket isdlayout bei. Die aktuellste Version (sowie die Anleitung zur Installation, um das Paket global auf dem Rechner zu nutzen, 
ohne es immer von Ordner zu Ordner kopieren zu müssen), findet man [in diesem Repository](https://bitbucket.org/frifrank/isd-tools/src/master/). 