\documentclass[aspectratio=169, 10pt]{beamer}

\usepackage{isdbasics}
\usepackage{isdlayout}
\usepackage{lipsum}
%\usepackage{todonotes}
%\setuptodonotes{inline, size=\scriptsize}
\usepackage{caption}

% \usepackage{xcolor}
% \usepackage[absolute,overlay]{textpos}
\include{00_Praeambel/01_Acronyms}

\usetikzlibrary{positioning, chains, fit, arrows.meta, spy}

\title{Erosion an Verdichterschaufeln}
\subtitle{Ansatz zur Simulation mithilfe eines Phasenfeldmodells}
\date{\today}
\author{F. Bartel, M. Brodbeck, F. Frank, M. Lorenz}

\tikzset{
  >={Latex[]},
  nosep/.style={inner sep=0pt, outer sep=0pt},
  fcnode/.style={inner sep=5pt, outer sep=2pt, rectangle, rounded corners=3pt, align=center, very thick},
  prog/.style={fcnode, draw=isdblue,},
  inp/.style={fcnode, draw=isdgreen,},
  fitnode/.style={draw=isdgray, thick, dotted, rectangle, rounded corners=5pt,},
}

\tikzset{
    invisible/.style={opacity=0,text opacity=0},
    visible on/.style={alt={#1{}{invisible}}},
    alt/.code args={<#1>#2#3}{%
      \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
    },
  }
  
%% Bibliograpie
\usepackage[backend=biber, style=ieee]{biblatex}
\addbibresource{01_Bibliographie/Bib_Phasefield.bib}
\setbeamertemplate{bibliography item}{\insertbiblabel}

\begin{document}
  \maketitle 

  \begin{frame}
    \frametitle{Inhalt}
    \tableofcontents
  \end{frame}

\section{Motivation und aktueller Stand}

\begin{frame}
  \frametitle{Motivation}
  \begin{itemize}
    \item Modellierung von Erosion aufgrund vieler Einflussgrößen ist komplex 
    \item Einflussgrößen z.B.
      \begin{itemize}
        \item Partikelgröße
        \item Form und Material
        \item Schaufelmaterial
        \item Partikelgeschwindigkeit
        \item Einschlagswinkel
        \item \dots
      \end{itemize}
    \item Es gibt noch keinen universell anwendbaren Ansatz.
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Modellierung am ILA}

  \begin{columns}
    \begin{column}{.6\linewidth}
      \begin{itemize}
        \item Schaufel als Punktewolke
        \item Berechnung der Versetzung von linearen Segmenten zwischen den Punkten
        \item Erosionsrate aus Versuchen
        \item Versetzungslänge abhängig von Partikeldurchsatz, Zeitschritt und Erosionsrate
        \item Versetzung entlang Normalenvektor
        \item Partikel als kontinuierliche Phase modelliert. \linebreak[3] Keine Einzelpartikelrechnung.
      \end{itemize} 
    \end{column}
    \begin{column}{.3\linewidth}
      \includegraphics[width=.8\linewidth]{img/erosionsmodell.png}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Versuchsaufbau und Messdaten}
  \begin{itemize}
    \item Sandstrahl Prüfstand am ILA: Bekannte Partikelmasse wird in den Prüfstand eingebracht und ein Prüfkörper bestrahlt.
    \item Oberfläche der Prüfkörper schrittweise mittels 3D-Scanner vermessen
    \item Messdaten gegen Geometriemodellierung
    \begin{center}
      \begin{tikzpicture}
        \node[anchor=west] (a) at (0,0) {\includegraphics[height=2.5cm]{img/erosion_schaufel_netz.png}};
        \node[anchor=east] (b) at (.8\linewidth,0) {\includegraphics[height=2.5cm]{img/erosion_schaufel_schnitt.png}};
        \node[anchor=north east, font=\scriptsize, nosep, xshift=-5pt] at (b.south east) {Quelle: ILA-Dissertation Schrade};
        \draw[-{Latex[]}, ultra thick] (a) -- (b);
      \end{tikzpicture}
    \end{center}

    \item \setstackEOL{\\}\Longunderstack[l]{Erosion an Einzelschaufeln\\(Sandstrahl Prüfstand)} \quad$\rightarrow$\quad \Longunderstack[l]{Verdichterkaskade\\(Abgewickelte Schaufelreihe)} \quad$\rightarrow$\quad Stufe/ Triebwerk
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Rolle der numerischen Simulation}

  \begin{itemize}
    \item Simulation der Erosion mithilfe der Finite Elemente Methode
    \item Implementierung eines Schadensmodells ("Phasenfeldmodell")
    \item Erste Annahmen (entspricht der ersten experimentellen Untersuchung): 
      \begin{itemize}
        \item Vernachlässigung aerodynamischer Effekte
        \item Einzelner (normalverteilter) Partikelstrahl
        \item kontinuierliche Randbedingung (keine Einzelpartikelbetrachtung)
      \end{itemize} 
  \end{itemize}
\end{frame}



\begin{frame}
  \frametitle{Ablauf der Erosionsanalyse}
  \centering
  \begin{tikzpicture}

    \begin{scope}[minimum width=4cm, start chain=1 going right,node distance=8mm,   every join/.style={draw, thick, -Latex[], isdblue},]
      \node[prog, on chain=1, join] (apdl) {Ansys APDL Skript};
      \node[prog, on chain=1, join] (feap) {FEAP User Element};
      \node[prog, on chain=1, join] (para) {Parallelisierung};
    \end{scope}

    \begin{scope}[continue chain=1 going above, node distance=2mm, font=\small]
      \node[inp, on chain] at (apdl.north) (rb) {Randbedingungen};
      \node[inp, on chain] (nparam) {Netzparameter};
      \node[inp, on chain] (profg) {Profilgeometrie};  
      \node[inp, on chain] at (feap.north) (mp) {Materialparameter};
      \node[inp, on chain] (ls) {Lösungsstrategie};
    \end{scope}

    \node[fitnode, fit=(apdl)(profg)] {};
    \node[fitnode, fit=(feap)(ls)] {};

    \node[below=1mm of apdl, nosep, align=center, font=\small, text width=4cm] {
    \setlength{\leftmargini}{10pt}
    \begin{itemize}
      \item Netzgenerierung, Randbedingungen
      \item Export zu FEAP
    \end{itemize}};

    \node[below=1mm of feap, nosep, align=center, font=\small, text width=4cm] {
      \setlength{\leftmargini}{10pt}
      \begin{itemize}
        \item ein/zwei zusätzliche Freiheitsgrade pro Knoten (modellabhängig)
      \end{itemize}};

    \node[below=1mm of para, nosep, align=center, font=\small, text width=4cm] {
      \setlength{\leftmargini}{10pt}
      \begin{itemize}
        \item Berechnung auf HLRS 
        \item Parallelisierte Version von FEAP (ParFEAP)
      \end{itemize}};
  \end{tikzpicture}
\end{frame}

\section{Geometrie- und Netzgenerierung mit APDL}

\begin{frame}
  \frametitle{Schaufelgeometrie}
  \begin{columns}
    \begin{column}[b]{.55\linewidth}
      \begin{itemize}
        \item Betrachtete Verdichtergeometrie: Nasa Rotor 37
        \item Profil annähernd als symmetrisch zu betrachten
        \item Ausschließlich Betrachtung der Schaufelvorderkante (Leading Edge)
      \end{itemize}     
      \vspace{.5cm}
      \begin{figure}
        \includegraphics[width=\linewidth]{img/blade_profile_2.jpg}
        \caption{Profil des Nasa Rotor 37. Die Anströmung erfolgt von links}
      \end{figure}
    \end{column}
    \begin{column}[b]{.3\linewidth}
      \begin{figure}
        \includegraphics[width=\linewidth]{img/blade_total.png}
        \caption{NASA Rotor 37}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Netzgenerierung}

  \begin{columns}
    \begin{column}{.4\linewidth}
      \begin{itemize}
        \item Netzeigenschaften werden vom Nutzer festgelegt
        \begin{itemize}
          \item Netzgröße
          \item Größenübergang
          \item Netzdichte auf der Oberfläche
          \item \dots
        \end{itemize}
        \item Höhere Netzdichte in der Mitte (maximale "Partikellast")
      \end{itemize}
    \end{column}
    \begin{column}{.6\linewidth}
      \includegraphics[width=\linewidth]{img/netz_ges.png}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Netzeigeschaften im Zentrum des Profils}
  \centering
  \begin{tikzpicture}[
        spy scope={magnification=3, size=5cm},
        every spy in node/.style={
        draw, ultra thick, circle}]

    \node (imga) {\includegraphics[width=.45\linewidth]{img/netz_tip.png}};
    \node[right=of imga] (imgb) {\includegraphics[width=.45\linewidth]{img/netz_cut.png}};
    \only<2->{\spy [isdgray, overlay] on (-.65,-.3) in node;} % at (4,1.8);}
    \only<3->{\spy [isdgray, overlay] on ($(imgb)+(-.1,-.9)$) in node;}%  at (4,-1.8);}
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Randbedingungen}
  \begin{columns}
    \begin{column}{.6\linewidth}
      \begin{itemize}
        \item Randbedingungen auf bestimmte Bereiche der Oberfläche
        \item Lasten in Abhängigkeit von Funktionen in alle Raumrichtungen
          \[ f(x, y, z) = f_{x}(x) \cdot f_{y}(y) \cdot f_{z}(z) \]
        \item Standard: Normalverteilungen, um (zylindrischen) Partikelstrahl abzubilden
        \item Integration über Flächenelemente und Aufteilung auf Knoten
        \item Übergang von Flächen- auf Knotenlasten momentan nur für lineare Elemente exakt
      \end{itemize}
    \end{column}
    \begin{column}{.4\linewidth}
      \includegraphics[width=\linewidth]{img/netz_tip_f.png}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Export zu FEAP}
  \begin{itemize}
    \item Skript exportiert Knoten, Elemente, Randbedingungen
    \item Generiert Dateien für weitere Verwendung mit FEAP und\dots
    \item Generiert Dateien für Verwendung auf HLRS 
  \end{itemize}
\end{frame}

\section{Theoretische Grundlagen Phasenfeld}
\begin{frame}
  \frametitle{Literatur}
  \nocite{*}
  \printbibliography[heading=none]
  \end{frame}
\begin{frame}
  \frametitle{Geometrische Motivation Regularisierung}
  \begin{columns}
    \begin{column}{.55\linewidth}
      \textbf{Ausgangssituation:}
      \begin{itemize}
        \item Betrachtung unendlicher Balken mit Querschnitt \(\CrossSect\)
        \item Annahme: Schädigung (Riss) an Stelle \(x=0\)
      \end{itemize}
    
      \textbf{Modellierung:}
      \begin{itemize}
        \item Einführen Hilfsgröße \(\PhaseD\), wobei \(\PhaseD=1\) vollständige Schädigung anzeigt
        \item Approximation Verlauf mit \(\PhaseD (x)=\exp(-\abs{x}/\ell) \)
      \end{itemize}
    \end{column}

    \begin{column}{.45\linewidth}
      \begin{figure}
        \begin{tikzpicture}[scale=.8]
          \begin{scope}
            \draw[->] (-3,0) node[above=.5] {a)} -- (3,0) node[above] {$x$};
            \draw[->] (0,0) node[below] {0} -- (0,2) node[right=.2] {$d(x)$};
            \draw (0.1,1.5) -- ++(-.2,0) node[left]{1};
            \draw[isdblue, ultra thick] (-2.2,0) -- (2.2,0) (0,0) -- (0,1.5);
            \draw[isdblue, ultra thick, dotted] (-2.2,0) -- ++(-.5,0)  (2.2,0) -- ++(0.5,0);
          \end{scope}

          \begin{scope}[shift={(0,-2.8)}]
            \draw[->] (-3,0) node[above=.5] {b)} -- (3,0) node[above] {$x$};
            \draw[->] (0,0) node[below] {0} -- (0,2) node[right=.1] {$d(x)$};
            \draw (0.1,1.5) -- ++(-.2,0) node[left]{1};
            \pgfmathsetmacro{\ang}{20}
            \pgfmathsetmacro{\diag}{1.5/cos(\ang)}
            \draw[isdblue, ultra thick, line join=bevel, looseness=1.6] (-2.2,0) to[out=0, in={-90-\ang}] (0,1.5) to[out={-90+\ang}, in=180] (2.2,0);
            \draw[isdblue, ultra thick, dotted] (-2.2,0) -- ++(-.5,0)  (2.2,0) -- ++(0.5,0);
            \draw[densely dotted, thick] (0,1.5) ++({-90-\ang}:\diag) node[below, align=center]{\makebox[\widthof{$\ell$}][r]{$-\ell$}} -- (0,1.5) -- ++({-90+\ang}:\diag) node[below, align=center] {$\ell$};
          \end{scope}
        \end{tikzpicture}
        \caption[center]{Vergleich Riss a) und regularisiertem Riss b)}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Geometrische Motivation Regularisierung}
  \textbf{Mathematische Formulierung:}
  \begin{itemize}
    \item $\PhaseD (x)$ ist Lösung der DGL
    \begin{equation*}
      d(x)-\PhaseL ^2\cdot\PhaseD'' = 0
    \end{equation*}
    und damit des Variationsproblems
    \begin{equation*}
      d=\Arg\left\{\inf I(\PhaseD) \mid \PhaseD(0)=1,\; \PhaseD(\pm\infty)=0 \right\}
    \end{equation*}     
    \item Einsetzen Exponentialansatz für $\PhaseD $ liefert $I(\PhaseD )=\PhaseL\:\SurfCrack $\quad $\leftrightarrow$\quad Minimierung der Rissfläche
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Ableitung beschreibender Gleichungen}
    \begin{itemize}
      \item Ziel: Ableitung (gekoppelter) Transportgleichungen für Verschiebung und beschreibenden Größen des Phasenfelds
      \item Dazu: Prinzip der virtuellen Leistung, mit homogenen Dirichlet-RB für die Raten auf dem Rand
      \begin{equation*}
      \InternalPower+\Dissipation = \ExternalPower
      \end{equation*} 
    \end{itemize}
    \begin{center}
      \begin{tabular}{|c| l|}
        \hline
        $\InternalPower$ & Zeitl. Evolution gespeicherte Energie \\
        $\Dissipation$ & Dissipierte Leistung \\
        $\ExternalPower$ & Leitung aus äußeren Lasten \\
        \hline
      \end{tabular}
    \end{center}
\end{frame}

\begin{frame}
  \frametitle{Ableitung beschreibender Gleichungen}
  \textbf{Bestimmung der Dissipation} $\mathbf{\Dissipation}$
  \begin{itemize}
    \item Erweiterung der Minimierung der Rissfläche auf mehrere Dimensionen mit
    \begin{equation*}
    \SurfCrack = \int\underbrace{\frac{1}{2\PhaseL}\cdot\left(\PhaseD ^2+\PhaseL ^2\:(\Grad{\PhaseD})^T\;\Grad{\PhaseD}\right)}_{\text{Rissdichte}\;\SurfCrackDens}\; \dVol \qquad |\;\Grad{\PhaseD}\cdot\Vek{n}=0
    \end{equation*}
    \item Dissipation aus zeitlicher Änderung Rissfläche und Arbeit, die bei Entstehung geleitet wird
    \begin{equation*}
    \Dissipation = \int\chi\;\dVol\qquad |\;\chi=\underbrace{\DissipTresh\dot{\SurfCrackDens}}_{>0} + \, [\dots]
    \end{equation*}
          
  \end{itemize}
\end{frame}

\section{Implementierung des User-Elements in FEAP}
\begin{frame}
  \frametitle{Ableitung beschreibender Gleichungen}
  \frametitle{Implementierung}
  \textbf{Aufbau und Modellannahmen:}
    \begin{itemize}
      \item Kopplung des Phasenfelds an einen linear elastischen Festkörper
      \item Phasenfeld in Zwei- bzw. Dreifeldformulierung ohne Berücksichtigung von Zug/Druck Verhalten implementiert
      \item Zeitdiskretisierung mit Newmark-Verfahren
    \end{itemize}
    \textbf{Arbeitsstand und Next-Steps:}
    \begin{itemize}
      \item Implementierung abgeschlossen
      \item Validierung begonnen, Testfälle aus Paper stehen aber noch aus
      \item Aufgrund hoher Diskretisierungsanforderung parallele Berechnung benötigt
    \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Wahl der Randbedingungen}
  \begin{itemize}
    \item Für reines Erosionsproblem: Keine Spannung im Bauteil $\leftrightarrow$ Phasenfeld dissipiert Energieeintrag der Partikel
    \item $\DissipTresh\Grad{\PhaseD}\cdot\Vek{n}$ stellt diffusiven Energiefluss über Oberfläche dar $\leftrightarrow$ Kann so Eintrag aus Partikeln simuliert werden?
  \end{itemize}
  
  \textbf{Offene Punkte:}
  \begin{itemize}
    \item Bestimmung des Wertes für $\DissipTresh$
    \item Verteilung der eingetragenen Energie über Oberfläche
    \item Welcher Anteil der kinetischen Energie der Teilchen wird in Körper eingetragen
  \end{itemize}
\end{frame}

\section{Parallelisierung am HLRS}
\begin{frame}
  \frametitle{ParFEAP}
  \begin{itemize}
    \item Lange Laufzeiten bei serieller Berechnung
    \item ParFEAP als parallele Variante 
    \item Partitionierung des Rechengebiets und Aufteilung auf mehrere Prozessorkerne
  \end{itemize}
  \begin{columns}
    \begin{column}{.6\linewidth}
      \includegraphics[width=\linewidth]{img/netz_partitions.png}
    \end{column}
    \begin{column}{.4\linewidth}
      \captionof{figure}{Partitionierung des Netzes. Überlagerte Bereiche stellen \emph{Ghost}-Elemente dar.}
    \end{column}
  \end{columns}
\end{frame}

\end{document}